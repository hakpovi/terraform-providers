terraform {
  required_providers {
    argocd = {
      source = "oboukili/argocd"
      version = "6.1.1"
    }
  }
}

provider "argocd" {
  # Configuration options
  port_forward = "true"
  username    = "admin"
  password    = "EmXOOlLWs-59XBH3"
}

resource "argocd_application" "helm" {
  metadata {
    name      = "helm-app"
    namespace = "argocd"
    labels = {
      test = "true"
    }
  }

  spec {
    destination {
      server    = "https://kubernetes.default.svc"
      namespace = "default"
    }

    source {
      repo_url        = "https://gitlab.com/hakpovi/prometheus.git"
      target_revision = "main"
      path = "." 
      helm {
        release_name = "testing"
        value_files = ["values.yaml"]
          }
      }
    }
  }
